
#include <Arduino.h>
#include <Servo.h>
#include "FastLED.h"
#include "MegunoLink.h"
#include "Filter.h"

#define N_PIXELS 42
#define MIC_PIN A0
#define LED_PIN D5
#define SERVO_PIN D2
#define SWITCH_PIN D7
#define NOISE 600
#define TOP (N_PIXELS+2)

Servo servo;
CRGB leds[N_PIXELS];
ExponentialFilter<long> ADCFilter(7,0);
int MENU = 0;
int lvl = 0, minLvl = 0, maxLvl = 120;

void workstate();
void chillstate();
void off();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  servo.attach(SERVO_PIN);
  pinMode(SWITCH_PIN, INPUT_PULLUP);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 600);
  FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, N_PIXELS);

}

void loop() {
  // put your main code here, to run repeatedly:
  int n, height;
  int sensor = digitalRead(D7);
  n = analogRead(MIC_PIN);
  n = abs(1023 - n);
  n = (n <= NOISE) ? 0 : abs(n - NOISE);
  ADCFilter.Filter(n);
  int lvl = ADCFilter.Current();
  Serial.println(n);
  Serial.println(" ");  
  Serial.println(lvl);     

  height = TOP * (lvl - minLvl) / (long)(maxLvl - minLvl);
  if(height < 0L) height = 0;
  else if(height > TOP) height = TOP;
  // turn the LEDs corresponding to the level on/off
  for(uint8_t i = 0; i < N_PIXELS; i++) {
    // turn off LEDs above the current level
    if(i >= height) leds[i] = CRGB(0,0,0);
    // otherwise, turn them on!
    else leds[i] = Wheel( map( i, 0, N_PIXELS-1, 105, 200 ) );
  }
  FastLED.show();        


  // if (sensor == HIGH){
  //   MENU = MENU + 1;
  //   if (MENU == 3){
  //     MENU = 0;
  //   }
  // }

  // if (MENU == 0){
  //   chillstate();
  //   //Serial.println("MENU : Chillstate");

  // }  
  
  // if (MENU == 1) {
  //   workstate();
  //   //Serial.println("MENU : Workstate");
  // }

  // if (MENU == 2) {


  // }
 

}

void workstate(){
  servo.write(180);
  fill_solid(leds, 42, CRGB::WhiteSmoke);
  FastLED.setBrightness(120);
  FastLED.show();
}

void chillstate(){
  servo.write(0);
  fill_solid(leds, 42, CRGB::BlueViolet);
  FastLED.setBrightness(60);
  FastLED.show();
}

void off(){
  servo.write(80);
  fill_solid(leds, 42, CRGB::Black);
  FastLED.show();
}

CRGB Wheel(byte WheelPos) {
  // return a color value based on an input value between 0 and 255
  if(WheelPos < 85)
    return CRGB(WheelPos * 3, 255 - WheelPos * 3, 0);
  else if(WheelPos < 170) {
    WheelPos -= 85;
    return CRGB(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
    WheelPos -= 170;
    return CRGB(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}