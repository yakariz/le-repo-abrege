
# Boitier capteur PIR et luminosité 

Boitier 45x45x41 mm acceptant les 3 composants necessaires à la détection de présence et intensité lumineuse (en lux). Facilement ajoutable avec l'integration esphome de Home Assistant (06:30): 





## Composants
Liens amazon prime (Total ≃ 10€ par boitier):
- [Capteur HC-SR501](https://amzn.to/3PNRwue)
- [Capteur BH 1750](https://amzn.to/3VmFERl)
- [D1 mini](https://amzn.to/3POoIln)

Liens aliexpress (Total ≃ 10€ par boitier):
- [Capteur HC-SR501](https://s.click.aliexpress.com/e/_DETbM91)
- [Capteur BH 1750](https://s.click.aliexpress.com/e/_DkvTUCr)
- [D1 mini](https://s.click.aliexpress.com/e/_DlUn36f)



## Connections 


- HC-SR501
```bash
  -> 5V
  -> GND
  -> D6
```
- BH1750
```bash
  -> 3.3V
  -> GND
  -> D1 (SCL)
  -> D2 (SDA)
```

## Filaments

| Couleur            | Hex                                                               | Filament                                                           |
| ----------------- | ------------------------------------------------------------------ | ------------------------------------------------------------------ |
| Boitier| ![#ffffff](https://via.placeholder.com/10/ffffff?text=+)| [Marbre](https://www.wanhaofrance.com/products/pla-premium-marbre-1kg-1-75-ender3-creality-wanhao?ref=abrege) 
| Face avant | ![#0a192f](https://via.placeholder.com/10/0a192f?text=+) | [Noir mat](https://amzn.to/3jr6tGO)



## Auteurs

- [@tsoriano_geneva](https://gitlab.com/tsoriano_geneva)

