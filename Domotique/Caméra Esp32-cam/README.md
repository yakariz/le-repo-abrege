
# Boitier Camera ESP32-CAM

Boitier 60x90x47 mm acceptant le module esp32-cam-mb. Facilement ajoutable avec l'intégration esphome de Home Assistant (version Novembre 2022 conseillé). Vidéo tuto (01:15): 





## Composants
Liens amazon prime (Total ≃ 16€ par boitier):
- [Esp32-CAM-MB](https://amzn.to/3js20DF)
- [Aimants](https://amzn.to/3BYryyv)


Liens aliexpress (Total ≃ 13€ par boitier):
- [Esp32-CAM-MB](https://fr.aliexpress.com/item/1005004005700341.html?)
- [Aimants 5x2](https://amzn.to/3BYryyv)



## Connections 

Uniquement besoin d'un cable micro usb.

## Filaments

| Couleur            | Hex                                                               | Filament                                                           |
| ----------------- | ------------------------------------------------------------------ | ------------------------------------------------------------------ |
| Boitier| ![#ffffff](https://via.placeholder.com/10/ffffff?text=+)| [Marbre](https://www.wanhaofrance.com/products/pla-premium-marbre-1kg-1-75-ender3-creality-wanhao?ref=abrege) 
| Eye | ![#0a192f](https://via.placeholder.com/10/0a192f?text=+) | [Noir mat](https://amzn.to/3jr6tGO)
| Cercle | ![#ffffff](https://via.placeholder.com/10/ffffff?text=+) | [Blanc](https://www.filimprimante3d.fr/filament-pla-175-mm/3245-filament-premium-pla-175mm-blanc-arctique-1kg-spectrum.html)



## Auteurs

- [@tsoriano_geneva](https://gitlab.com/tsoriano_geneva)

